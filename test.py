from wing import Wing
from plane import Plane
from environment import Environment
from os import listdir
from os.path import isfile, join
from xfoil_wrapper import OutOfRangeError, DataExtractionError

FOILS_PATH = "foils"
DROP_HEIGHT = 40
TURN_RADIUS = 20

w = Wing(   width = 50 * 0.0254,
            length = 5 * 0.0254,
            airfoil = "foils/rg15.dat" )

p = Plane(  wing = w,
            mass = 40 * 0.0283495 )

e = Environment(plane=p, start_height=DROP_HEIGHT, uplift=2)


p.dive_distance(DROP_HEIGHT-TURN_RADIUS)
print p.time, p.velocity, p.x, p.y
p.turn_vertical_to_horizontal(TURN_RADIUS)
print p.time, p.velocity, p.x, p.y
p.turn_horizontal_to_vertical(TURN_RADIUS)
print p.time, p.velocity, p.x, p.y
p.climb_to_top()
print p.time, p.velocity, p.x, p.y

print "Difference from start height", DROP_HEIGHT - p.y
print "Required Lift", (DROP_HEIGHT - p.y)/p.time
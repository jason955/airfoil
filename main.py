from os import listdir
from os.path import isfile, join
from xfoil_wrapper import OutOfRangeError, DataExtractionError
import sys


def simulate_drop_and_turn(plane, drop_height, turn_radius):
    error = ""
    print plane.wing.airfoil, "w:", plane.wing.width, "l:", plane.wing.length, "m:",plane.mass, "h:", drop_height, "r:", turn_radius, "t:", plane.time_segment
    try:
        t1,t2,t3,t4,v1,v2,v3,v4 = (0,0,0,0,0,0,0,0)
        plane.dive_distance(drop_height-turn_radius)
        t1, v1 = plane.time,plane.velocity
        plane.turn_vertical_to_horizontal(turn_radius)
        t2, v2 = plane.time,plane.velocity
        plane.turn_horizontal_to_vertical(turn_radius)
        t3, v3 = plane.time,plane.velocity
        plane.climb_to_top()
        t4, v4 = plane.time,plane.velocity
        

        #print "Difference from start height", drop_height - p.y
        #print "Required Lift", (drop_height - p.y)/p.time
        #print plane.velocity, plane.time

    except OutOfRangeError:
        error = error + "OOR;"
        print "out of range " + plane.wing.airfoil

    except DataExtractionError:
        error = error + "DE;"
        print "data extraction error " + plane.wing.airfoil

    except:
        error = error + "?;"
        print "unkown error " + plane.wing.airfoil

    finally:
        with open("output.txt",'a') as f:
            data = [plane.wing.airfoil,
                    t4,
                    t3,
                    t2,
                    t1,
                    v4,
                    v3,
                    v2,
                    v1,
                    plane.x, 
                    plane.y, 
                    drop_height, 
                    turn_radius,
                    plane.wing.width,
                    plane.wing.length,
                    plane.mass,
                    plane.time_segment,
                    error ]
            data = [str(i) for i in data]
            f.write( "\t".join(data))
            f.write("\n")


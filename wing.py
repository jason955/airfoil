from wing_section import WingSection
from constants import *
from xfoil_wrapper import get_data_at_cl
from os.path import join

MINIMUM_REYNOLDS = 1000

class Wing(object):
    airfoil_dir = 'foils'
    def __init__(self, width, length, airfoil=None):
        self.width = width # width m
        self.length = length # chord length m
        self.airfoil = airfoil

    @property
    def aspect_ratio(self):
        return self.width*1.0/self.length

    @property
    def area(self):
        return self.width*1.0*self.length

    def get_reynolds_number(self, velocity):
        r = int((AIR_DENSITY * abs(velocity) * self.length) / (AIR_VISCOSITY))
        if r < MINIMUM_REYNOLDS:
            return MINIMUM_REYNOLDS
        return r

    def get_cd_for_cl(self,cl,reynolds):
        # get CD from wing and Reynolds #
        if not self.airfoil:
            print "error: no airfoil specified."
            return
        airfoil_path = join(self.airfoil_dir, self.airfoil)
        result = get_data_at_cl(airfoil_path,reynolds,cl)
        #print self.airfoil,reynolds,cl,result
        return result['data'][0]['CD']




    
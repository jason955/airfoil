import multiprocessing
from os import listdir
from os.path import isfile, join
from main import simulate_drop_and_turn
import sys
from wing import Wing
from plane import Plane

if len(sys.argv)>1:
    queue_path = sys.argv[1]
else:
    queue_path = "queue.txt"

FOILS_PATH = "foils"

def parse_queue(filename):
    with open(filename,"r") as f:
        lines=f.readlines()

    parsed_items = []
    for line in lines[1:]:
        items = line.split("\t")
        parsed_items.append(
            {   "foil": items[0],
                "wing_w": float(items[1]),
                "wing_chord": float(items[2]),
                "mass": float(items[3]),
                "drop_height": float(items[4]),
                "turn_radius": float(items[5]),
                "time_segment": float(items[6])
            } )
    return parsed_items
            

if __name__ == '__main__':

    queue = parse_queue("queue.txt")
    #airfoils = [join(FOILS_PATH, f) for f in listdir(FOILS_PATH) if isfile(join(FOILS_PATH, f)) and ".dat" in f]
    pool = multiprocessing.Pool(processes=4)

    for item in queue:

        w = Wing(   width = item["wing_w"] * 0.0254,
                    length = item["wing_chord"] * 0.0254,
                    airfoil = item["foil"] )
        p = Plane(  wing = w,
                    mass = item["mass"] * 0.0283495,
                    time_segment = item["time_segment"])
        #simulate_drop_and_turn(p,item["drop_height"],item["turn_radius"])
        pool.apply_async(simulate_drop_and_turn, args=(p,item["drop_height"],item["turn_radius"]))
    
    pool.close()
    pool.join()




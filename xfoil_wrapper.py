import subprocess as sp
import os
import time
import re
import signal
import random, string
import sys


FNULL = open(os.devnull, 'w')
dir_path = os.path.dirname(os.path.realpath(__file__))
tmp_dir = os.path.join(dir_path,"tmp")
if not os.path.isdir(tmp_dir):
    os.mkdir(tmp_dir)

#XFOIL_PATH = "C:\Users\Jason\Documents\dev\airfoil\bin\xfoil.exe"

if sys.platform == 'win32':
    XFOIL_PATH = os.path.join(dir_path,"bin","xfoil.exe")
    SHELL = True
elif sys.platform == 'darwin':
    XFOIL_PATH = "/Applications/Xfoil.app/Contents/Resources/xfoil"
    SHELL = None
elif sys.platform == 'linux2':
    XFOIL_PATH = os.path.join("/", "usr","bin","xfoil")
    SHELL = None

print sys.platform

class OutOfRangeError(Exception):
    pass

class DataExtractionError(Exception):
    pass


def randomword(length):
   return ''.join(random.choice(string.lowercase) for i in range(length))


def delete_files(files):
    for f in files:
        try:
            os.remove(f)
        except:
            print "error deleting file"

def extract(filename):
    try:
        with open(filename,"r") as f:
            lines=f.readlines()
    except IOError:
        raise DataExtractionError
    # get foil name data
    result = re.search("Calculated polar for: (.+)",lines[3])
    foil_name = result.group(1).strip()
    # get Reynolds number
    result = re.search("Re = \s+(\S+) e (\d+)",lines[8])
    re_base = float(result.group(1))
    re_exp = int(result.group(2))
    reynolds = int(re_base * 10**re_exp)
    # get data
    data = []
    for line in lines[12:]:
        l = line.strip()
        cols = re.split('\s+',l)
        if len(cols)>1:
            try:
                data.append({'alpha':float(cols[0]),"CL":float(cols[1]),"CD":float(cols[2])})
            except:
                raise OutOfRangeError
    if not len(data):
            raise OutOfRangeError
    return {"foil_name":foil_name, "reynolds":reynolds, "data":data}




def get_data_at_cl(airfoil,reynolds,cl):
    try:
        return try_to_get_data_at_cl(airfoil,reynolds,cl)
    except OutOfRangeError:
        s = 0.1
        for i in [s/i for i in range(1,20)] + [random.random()/4 for i in range(20)]:
            try:
                #print "converging from " + str(i)
                return try_to_get_data_at_cl(airfoil,reynolds,cl,i)
            except OutOfRangeError:
                pass
    raise OutOfRangeError



def try_to_get_data_at_cl(airfoil,reynolds,cl,cl_start=None):
    rand_word = randomword(5)
    output_file = os.path.join(dir_path,"tmp",rand_word+".txt")
    dump_file = os.path.join(dir_path,"tmp",rand_word+"_dump.txt")
    log_file = os.path.join(dir_path,"tmp",rand_word+"_log.txt")

    commands = ["PLOP",
                "G",
                "",
                "LOAD " + airfoil,
                "OPER",
                "ITER 300",
                "VISC " + str(reynolds),
                "MACH 0"]
    if cl_start:
        commands.append("CL " + str(cl+cl_start))
    commands = commands + [ "PACC",
                            output_file,
                            dump_file,
                            "CL " + str(cl),
                            "PACC",
                            "",
                            "QUIT",
                            "" ]
    #print commands
    with open(log_file,"w") as f:
        ps = sp.Popen(  [XFOIL_PATH],
                    stdin=sp.PIPE,
                    stdout=f,
                    stderr=None,
                    shell=SHELL )
    with open(os.path.join("tmp","output_log.txt"),"a") as f:
        f.write("\t".join([airfoil,str(reynolds),str(cl),log_file]))
        f.write("\n")
    
    for cmd in commands:
        ps.stdin.write(cmd + '\n')
    ps.stdin.close()
    ps.wait()
    tries = 0
    while not os.path.isfile(os.path.join(output_file)):
        time.sleep(0.25)
        tries = tries + 1
        if tries > 10:
            break
    result = extract(output_file)
    delete_files([output_file,dump_file,log_file])
    return result


if __name__ == "__main__":
    result = try_to_get_data_at_cl(os.path.join(dir_path,"foils","rg14.dat"),10000,0)
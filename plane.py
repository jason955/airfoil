from wing import Wing
from constants import *
import math

class Plane(object):
    def __init__(self, wing, mass, time_segment=0.1):
        self.wing = wing
        self.mass = mass # kg
        self.max_v = 0.0
        self.min_v = 0.0
        self.v_x = 0 # x velocity m/s
        self.v_y = 0 # y velocity m/s
        self.x = 0.0
        self.y = 0.0
        self.time = 0.0
        self.time_segment = time_segment

    @property
    def wing_loading(self):
        return self.mass/self.wing.area

    @property
    def velocity(self):
        return abs(math.sqrt(1.0*self.v_x**2+self.v_y**2))
    
    @property
    def wing_reynolds(self):
        return self.wing.get_reynolds_number(self.velocity)

    @property
    def v_angle(self):
        if self.v_x == 0 and self.v_y == 0:
            return -90
        if self.v_x ==0:
            if self.v_y>0:
                return 90
            else:
                return -90
        angle = math.degrees(math.atan(self.v_y/self.v_x))
        # if self.v_x < 0:
        #     angle = angle * -1.0
        # if angle < 0.01 and angle > -0.01:
        #     return 0
        return angle
    
    def dive_for_x_seconds(self,time):
        # dives straight down for an increment of time.
        cd = self._get_cd_for_cl(0) # get drag at 0 lift
        x_f,y_f = self._resolve_forces(0,cd)
        self._apply_forces(x_f,y_f,time)
        #self.y = self.y + (self.velocity * time)

    def dive_distance(self,height):
        # dive for a specified distance
        # convert velocity straight down
        self._shift_velocity(-90)
        time_start = self.time
        start_coords = (self.x,self.y)

        while self._get_distance(start_coords,(self.x,self.y)) < height:
            self.dive_for_x_seconds(self.time_segment)


    def turn_vertical_to_horizontal(self,turn_radius, direction="right"):
        # travel over a quarter of the circle
        if direction == "right":
            start_coords = (self.x,self.y)
            self._shift_velocity(0)
            self.turn(turn_radius)
            self.x = start_coords[0] + turn_radius
            self.y = start_coords[1] - turn_radius

    def turn_horizontal_to_vertical(self,turn_radius, direction="right"):
        if direction == "right":
            start_coords = (self.x,self.y)
            self._shift_velocity(90)
            self.turn(turn_radius)
            self.x = start_coords[0] + turn_radius
            self.y = start_coords[1] + turn_radius


    def turn(self,turn_radius):
        cl = self._get_required_cl_for_turn(turn_radius)
        start_coords = (self.x,self.y)
        turn_distance = turn_radius * math.pi * 2 * 0.25
        self._shift_velocity(0)
        while self._get_distance(start_coords,(self.x,self.y)) < turn_distance:
            cd = self._get_cd_for_cl(cl)
            x_f,y_f = self._resolve_forces(cl,cd,True,True)
            self._apply_forces(x_f,y_f,self.time_segment)
            #print x_f,y_f,self.x,self.y
            #print self.v_x, self.v_y
            

    def climb_to_top(self):
        self._shift_velocity(90)
        start_coords = (self.x,self.y)
        while self.v_y > 0:
            self.climp_for_x_seconds(self.time_segment)


    def climp_for_x_seconds(self,time):
        # dives straight down for an increment of time.
        cd = self._get_cd_for_cl(0) # get drag at 0 lift
        x_f,y_f = self._resolve_forces(0,cd)
        self._apply_forces(x_f,y_f,time)

    ## private methods ##


    def _get_required_cl_for_turn(self, turn_radius):
        accel = self.velocity**2 / turn_radius / ACCELERATION_G
        # AIR_DENSITY * ACCELERATION_G * 
        return (accel * self.mass * ACCELERATION_G) / (0.5*AIR_DENSITY*self.velocity**2 *self.wing.area )

    def _get_distance(self,p1,p2):
        # get distance between two points
        x1 = p1[0]
        y1 = p1[1]
        x2 = p2[0]
        y2 = p2[1]
        return math.sqrt((x2-x1)**2+(y2-y1)**2)

    def _shift_velocity(self, degrees, direction="right"):
        # changes the direction of the velocity to a specified angle and direction
        # 0 = horizontal, -90 straignt down
        if direction == "right":
            velocity = self.velocity
            if degrees == 0:
                self.v_x = velocity
                self.v_y = 0
            elif degrees == -90:
                self.v_x = 0
                self.v_y = -velocity
            elif degrees == 90:
                self.v_x = 0
                self.v_y = velocity
            else:
                print "shift angle not implemented 1"
        else:
            print "shift angle not implemented2"



    def _apply_forces(self,force_x,force_y,time):
        if force_x == 0:
            x_a = 0
        else:
            x_a = force_x/self.mass
        if force_y ==0:
            y_a = 0
        else:
            y_a = force_y/self.mass
        self.v_y += y_a * time 
        self.v_x += x_a * time 
        self.time += time
        self.x += self.v_x * time
        self.y += self.v_y * time
        #print "f_x,f_y,accel_x, accel_y ", force_x, force_y, x_a, y_a

    def _resolve_forces(self, cl, cd, ignore_gravity=False, ignore_lift=False):
        # compute lift drag and gravity in newtons
        lift = 0.5 * AIR_DENSITY * self.velocity**2 * self.wing.area * cl
        drag = 0.5 * AIR_DENSITY * self.velocity**2 * self.wing.area * cd
        gravity = ACCELERATION_G * self.mass
        #print lift, drag, gravity
        #print "V,L,D,G,v_angle, sin: ", self.velocity, lift, drag, gravity, self.v_angle, math.cos(math.radians(self.v_angle))
        if self.v_x >= 0:
            lift_x = math.sin(math.radians(self.v_angle)) * -1 * lift
            lift_y = math.cos(math.radians(self.v_angle)) * lift
            drag_x = math.cos(math.radians(self.v_angle)) * -1 * drag
            drag_y = math.sin(math.radians(self.v_angle)) * -1 * drag
        else:
            lift_x = math.sin(math.radians(self.v_angle)) * -1 * lift
            lift_y = math.cos(math.radians(self.v_angle)) * lift
            drag_x = math.cos(math.radians(self.v_angle)) * -1 * drag
            drag_y = math.sin(math.radians(self.v_angle)) * drag

        total_force_x = drag_x
        total_force_y = drag_y
        #print "lift: ", lift_x, lift_y, "drag: ", drag, drag_x, drag_y,"V: ", self.v_x, self.v_y

        if not ignore_lift:
            total_force_x += lift_x
            total_force_y += lift_y

        if not ignore_gravity:
            total_force_y -= gravity
        #print total_force_x, total_force_y
        #print "total force: ", total_force_x, total_force_y
        # else:
        #     total_force_x = -1.0 * ( math.sin(math.radians(self.v_angle)) * -1 * lift + \
        #                     math.cos(math.radians(self.v_angle)) * drag )
        #     total_force_y = math.cos(math.radians(self.v_angle)) * -1 * lift + \
        #                     math.sin(math.radians(self.v_angle)) * -1 * drag + \
        #                     gravity
        return (total_force_x,total_force_y)

    def _get_cd_for_cl(self,cl):
        return self.wing.get_cd_for_cl(cl,self.wing_reynolds)





    # @property
    # def required_lift(self):
    #     return (self.mass * ACCELERATION_G) /\
    #            (0.5*AIR_DENSITY * (self.velocity)**2 * self.area)

